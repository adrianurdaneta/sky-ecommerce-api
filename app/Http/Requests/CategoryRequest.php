<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:100',
            'picture' => [Rule::requiredIf($this->path() == 'api/category'), 'file'],
            'id_profit' => 'nullable'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nombre',
            'id_profit' => 'id del profit',
            'picture' => 'imagen de la categoria',
        ];
    }
}
