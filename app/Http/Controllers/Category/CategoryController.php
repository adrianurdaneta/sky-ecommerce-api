<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Models\Category;
use Exception;
use Carbon\Carbon;
use App\Utils\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->showAll(CategoryResource::collection(Category::get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        DB::beginTransaction();
        try {

            $date = new Carbon();
            $name = str_replace(' ', '-', $request->name);
            $category = Category::create([
                'name' => $request->name,
                'picture' => ($request->hasFile('picture')) ? $request->file('picture')->storeAs("categories", "$name-$date->timestamp.png", 'public') : null,
            ]);

            if(isset($request->id_profit)){
                $category->sync = 1;
                $category->id_profit = $request->id_profit;
                $category->save();
            } else { 
                $category->sync = 0;
            }

            DB::commit();
            
            if(isset($request->id_profit)){
                return response()->json([ 'data' => $category]);
            } else { 
                return $this->successfulResponse();
            }
            
        } catch (Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CategoryRequest  $request
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        DB::beginTransaction();
        try {
            $date = new Carbon();
            $category->name = $request->name;
            if ($request->hasFile('picture')) {
                $name = str_replace(' ', '-', $request->name);
                $this->deleteImage($category->picture);
                $category->picture = $request->file('picture')->storeAs("categories", "$name-$date->timestamp.png", 'public');
            }
            
			if(isset($request->id_profit)){
                $category->update(['sync' => 1, 'id_profit' => $request->id_profit ]);
            } else { 
                $category->update(['sync' => 0]);
            }

            DB::commit();
			
            if(isset($request->id_profit)){
                return response()->json([ 'data' => $category]);
            } else { 
                return response()->json([ 'data' => $request->name]);
            }
        } catch (Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        try {
            $this->deleteImage($category->picture);
            $category->delete();
            return $this->successfulResponse();
        } catch (Exception $e) {
            return $e;
        }
    }

    public function get_all()
    {
        try { 

            return $this->showAll(CategoryResource::collection(Category::where('sync', 0)->get()));

        } catch (Exception $e){ 
            return $e;
        }
    }

      public function confirm_profit(Request $request, Category $category)
    {
        try {

            for($i = 0; $i<count($request->all()); $i++){
                 Category::where('id', array($request[$i]['id']))
                        ->update([
                                    'sync' => 1,
                                    'id_profit' => $request[$i]['id_profit']
                                ]);
            }

            return $this->successfulResponse();
            
        } catch (Exception $e){
            return $e;
        }
    }


}
