<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\SubCategoryRequest;
use App\Http\Requests\UpSubCategoryRequest;
use App\Http\Resources\Category\SubCategoryResource;
use App\Models\Category;
use App\Models\SubCategory;
use App\Utils\CodeResponse;
use Exception;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = SubCategory::paginate($request->limit);
        return $this->showPaginated(SubCategoryResource::collection($response));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SubCategoryRequest  $request
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function store(SubCategoryRequest $request, Category $category)
    {
        try {
            if(isset($request->id_profit)){

                $categories = $category->subCategories()->create($request->validated());

                $categories->sync = 1;
                $categories->id_profit = $request->id_profit;
                $categories->save();

                return response()->json([ 'data' => $categories]);

            } else {

                $category->subCategories()->create($request->validated());
                $category->sync = 0;
                $category->save();

                return $this->successfulResponse(CodeResponse::CREATED);
            }
            
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SubCategoryRequest  $request
     * @param  SubCategory $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(SubCategoryRequest $request, SubCategory $subCategory)
    {
        try {
            $subCategory->update($request->validated());
            $subCategory->update(['sync' => 0]);
            return $this->successfulResponse();
        } catch (Exception $e) {
            return $e;
        }
    }

    public function updateProfit(UpSubCategoryRequest $request, SubCategory $subCategory)
    {
        try {
			
			$subCategory = SubCategory::where('id', $request->id)->first();
			
			$subCategory->update($request->validated());
            
			if(isset($request->id_profit)){
                $subCategory->update(['sync' => 1]);
            } else { 
                $subCategory->update(['sync' => 0]);
            }
			
            if(isset($request->id_profit)){
                return response()->json([ 'data' => $subCategory ]);
            } else { 
                return $this->successfulResponse();
            }
			
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  SubCategory $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subCategory)
    {
        try {
            $subCategory->delete();
            return $this->successfulResponse();
        } catch (Exception $e) {
            return $e;
        }
    }

    public function get_all()
    {
        try { 

            return $this->showAll(SubCategoryResource::collection(SubCategory::where('sync', 0)->get()));

        } catch (Exception $e){ 
            return $e;
        }
    }

      public function confirm_profit(Request $request, SubCategory $subcategory)
    {
        try {

           for($i = 0; $i<count($request->all()); $i++){
                SubCategory::where('id', array($request[$i]['id']))
                        ->update([
                                    'sync' => 1,
                                    'id_profit' => $request[$i]['id_profit']
                                ]);
            }

            return $this->successfulResponse();
            
        } catch (Exception $e){
            return $e;
        }
    }
}
