<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\Product\ProductResource;
use App\Models\Product;
use App\Models\Variation;
use App\Utils\CodeResponse;
use App\Utils\ImageTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = Product::query();

        if ($request->category)
            $response->whereHas('subCategory', fn ($query) => $query->where('category_id', $request->category));

        if ($request->subCategory)
            $response->whereSubCategoryId($request->subCategory);

        if ($request->name)
            $response->where(function (Builder $query) use ($request) {
                return $query->where('name', 'LIKE', "%$request->name%")->orWhereHas('subCategory', fn ($query2) => $query2->where('name', 'LIKE', "%$request->name%"))->orWhereHas('subCategory.category', fn ($query2) => $query2->where('name', 'LIKE', "%$request->name%"));
            });
        return $this->showPaginated(ProductResource::collection($response->paginate($request->limit)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        DB::beginTransaction();
        try {

            $date = new Carbon();
            $name = str_replace(' ', '-', $request->name);
            $product = Product::create([
                'name' => $request->name,
                'sub_category_id' => $request->sub_category,
                'picture' => ($request->hasFile('picture')) ? $request->file('picture')->storeAs("products", "$name-$date->timestamp.png", 'public') : null,
                'sync' => 0,
            ]);

            if(isset($request->id_profit)){
                $product->sync = 1;
                $product->id_profit = $request->id_profit;
                $product->save();
            }

            foreach ($request->variations as $row) :
                $product->variations()->create([
                    'size_id' => $row['size_id'],
                    'price' => $row['price'],
                    'sync' => 0,
                ]);
            endforeach;

            DB::commit();

            if(isset($request->id_profit)){
                return response()->json([ 'data' => $product]);
            } else { 
                return $this->successFulResponse();
            }
          
           
        } catch (Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest  $request
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        DB::beginTransaction();
        try {

            $date = new Carbon();
            $product->name = $request->name;
            $product->sub_category_id = $request->sub_category;

            if ($request->hasFile('picture')) {
                $name = str_replace(' ', '-', $request->name);
                $this->deleteImage($product->picture);
                $product->picture = $request->file('picture')->storeAs("products", "$name-$date->timestamp.png", 'public');
            }

            if(isset($request->id_profit)){
                $product->sync = 1;
                $product->id_profit = $request->id_profit;
            } else { 
                $product->sync = 0;
            }

            $product->saveOrFail();
            $product->variations()->delete();

            foreach ($request->variations as $row) :
                $product->variations()->create([
                    'size_id' => $row['size_id'],
                    'price' => $row['price']
                ]);
            endforeach;

            DB::commit();


             if(isset($request->id_profit)){
                return response()->json([ 'data' => $product]);
            } else { 
                return $this->successFulResponse();
            }

        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest  $request
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function updateProfit(ProductRequest $request, Product $product)
    {
        DB::beginTransaction();
        try {

            $date = new Carbon();
            $product->name = $request->name;
            $product->sub_category_id = $request->sub_category;

            if ($request->hasFile('picture')) {
                $name = str_replace(' ', '-', $request->name);
            }

            if(isset($request->id_profit)){
                $product->sync = 1;
                $product->id_profit = $request->id_profit;
            } else { 
                $product->sync = 0;
            }

            $product->saveOrFail();
			
			$variation = Variation::where('product_id', $product->id)->first();

            foreach ($request->variations as $row) :
                $variation->price = $row['price'];
                $variation->size_id =  $row['size_id'];
				break;
            endforeach;
			
			$variation->saveOrFail();

            DB::commit();

            if(isset($request->id_profit)){
                return response()->json([ 'data' => $product]);
            } else { 
                return $this->successFulResponse();
            }

        } catch (Exception $e) {
			DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            $product->delete();
            return $this->successfulResponse();
        } catch (Exception $e) {
            return $e;
        }
    }

    public function get_all()
    {
        try {

            return $this->showAll(ProductResource::collection(Product::where('sync', 0)->get()));

        } catch (Exception $e){

            return $e;

        }

    }

    
    public function confirm_profit(Request $request, Product $product)
    {
        try {

            for($i = 0; $i<count($request->all()); $i++){

                Product::where('id', array($request[$i]['id']))
                            ->update([
                                'sync' => 1,
                                'id_profit' => $request[$i]['id_profit']
                            ]);
            }

           return $this->successfulResponse();

        } catch (Exception $e){

            return $e;

        }
    }

}
