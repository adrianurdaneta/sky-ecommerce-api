<?php

namespace App\Http\Controllers\People;

use Illuminate\Database\Eloquent\Collection;
use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\PeopleRequest;
use App\Http\Resources\People\PeopleResource;
use App\Models\People;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = People::query();

        if (Auth::user()->user->isCustomer()) {
            $response->whereHas('user.roles', fn ($query) => $query->where('name', 'admin'));
        } else {
            if ($request->role) {
                $response->whereHas('user.roles', fn ($query) => $query->where('name', $request->role));
            }
        }

        if ($request->search)
            $response->whereHas('user', fn ($query) => $query->where('email', 'LIKE', "%$request->search%"))->orWhere('phone', 'LIKE', "%$request->search%");
        return $this->showPaginated(PeopleResource::collection($response->paginate($request->limit)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PeopleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(PeopleRequest $request)
    {
        DB::beginTransaction();
        try {

            $request['state_id'] = $request->state;
            Auth::user()->user->people()->update($request->only('state_id', 'firstname', 'lastname', 'phone'));
            Auth::user()->user->people()->update(['sync' => 0]);
            DB::commit();
            return $this->successfulResponse();
        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PeopleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(PasswordRequest $request)
    {
        try {
            Auth::user()->user()->update([
                'password' => Hash::make($request->password)
            ]);
            return $this->successfulResponse();
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Submit to get all to profit
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function get_all()
    {
        try {
               return $this->showAll(PeopleResource::collection(People::where('sync', 0)->get()));           
        } catch (Exception $e){
            return $e;
        }
         
    }

    /**
     * Change to status sync
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function confirm_profit(Request $request, People $people)
    {

        try {

            for($i = 0; $i<count($request->all()); $i++){
                People::where('id', array($request[$i]['id']))
                            ->update([
                                        'sync' => 1,
                                        'id_profit' => $request[$i]['id_profit']
                                    ]);
            }

           return $this->successfulResponse();

        } catch (Exception $e){
            return $e;
        }
    }
}
