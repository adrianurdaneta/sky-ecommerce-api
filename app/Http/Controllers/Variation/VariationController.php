<?php

namespace App\Http\Controllers\Variation;

use App\Http\Controllers\Controller;
use App\Models\Variation;
use App\Models\VariationSize;
use Illuminate\Http\Request;
use App\Http\Requests\VariationRequest;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Utils\CodeResponse;
use App\Http\Resources\Variation\VariationSizeResource;
use Illuminate\Database\Eloquent\Builder;

class VariationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VariationRequest $request)
    {
       try {

            if(isset($request->id_profit)){

                $variation = VariationSize::create($request->validated());
                $variation->id_profit = $request->id_profit;
                $variation->sync = 1;
                $variation->save();
                DB::commit();
                return response()->json([ 'data' => $variation]);

            } else { 

                VariationSize::create($request->validated());
                DB::commit();
                return $this->successfulResponse(CodeResponse::CREATED);

            }
        
        } catch (Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VariationSize  $variation
     * @return \Illuminate\Http\Response
     */
    public function show(VariationRequest $request, VariationSize $variation)
    {
        try { 
          

        } catch (Exception $e){
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VariationSize  $variation
     * @return \Illuminate\Http\Response
     */
    public function update(VariationRequest $request, VariationSize $variation)
    {
        try{ 

             if(isset($request->id_profit)){

                $variation->update($request->validated());
                $variation->update(['sync' => 0]);

                return response()->json([ 'data' => $variation]);

            } else { 

                $variation->update($request->validate());
                 
                return $this->successfulResponse();
            }

        } catch (Exception $e){
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Variation  $variation
     * @return \Illuminate\Http\Response
     */
    public function destroy(VariationSize $variation)
    {
        //
    }

       /**
     * Remove the specified resource from storage.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function get_all()
    {
        try {

            return $this->showAll(VariationSizeResource::collection(VariationSize::where('sync', 0)->get()));           
            
        } catch (Exception $e) {

            return $e;

        }
    }

    
    /**
     * Change to status sync
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function confirm_profit(Request $request, VariationSize $variationSize)
    {

        try {

            for($i = 0; $i<count($request->all()); $i++){
                VariationSize::where('id', array($request[$i]['id']))
                            ->update([
                                        'sync' => 1,
                                        'id_profit' => $request[$i]['id_profit']
                                    ]);
            }

           return $this->successfulResponse();

        } catch (Exception $e){
            return $e;
        }
    }
}
