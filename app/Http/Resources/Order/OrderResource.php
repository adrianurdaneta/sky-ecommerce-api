<?php

namespace App\Http\Resources\Order;
use App\Http\Resources\User\ShortUserResource;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'total' => $this->total,
            'status' => $this->status,
            'products' => $this->products,
            'user' => new ShortUserResource($this->user),
            'type' => $this->type->name,
            'payment' => ($this->payment) ? $this->payment->bank()->withTrashed()->first()->type : null,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
