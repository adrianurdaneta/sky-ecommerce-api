<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type_id' => 1,
            'user_id' => 1,
            'status' => Order::PENDING_FOR_PAYMENT,
            'total' => $this->faker->randomDigit,
            'total_bs' => $this->faker->randomDigit,
            'sync' => $this->faker->randomElement([0, 1]),
        ];
    }
}
