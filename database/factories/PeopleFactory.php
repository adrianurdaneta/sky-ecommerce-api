<?php

namespace Database\Factories;

use App\Models\State;
use App\Models\People;
use Illuminate\Database\Eloquent\Factories\Factory;

class PeopleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = People::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
  
        return [
            'state_id' => 1,
            'firstname' => $this->faker->name(),
            'lastname' =>$this->faker->name(),
            'phone' => '04262198512',
            'sync' => $this->faker->randomElement([1,0]),
        ];
    }
}
