<?php

namespace Database\Seeders;

use App\Models\People;
use Illuminate\Database\Seeder;

class PeopleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $peoplef = People::factory()
                                    ->has(
                                            \App\Models\User::factory()->count(1)
                                    )
                                    ->count(5)
                                    ->create();
    }
}
